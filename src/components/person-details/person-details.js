import React, { Component } from 'react';

import './person-details.css';
import SwapiService from "../../services/swapi-service";



export default class PersonDetails extends Component {
    swapi= new SwapiService();


state = {
    person : {

    },
}
componentDidMount() {
    this.interval=setInterval(this.updatePerson,1000)
    this.updatePerson();
}
    componentWillUnmount() {
        clearInterval(this.interval)
    }

updatePerson = () =>{
    const id = Math.floor(Math.random()*(83-1)+1);
    this.swapi.getPerson(id).then((data)=>{
        console.log(data)
        this.setState({
            person : data,
        })
    })
}

    render() {
      const {id,name,gender,eyeColor} = this.state.person;
      const url = `https://starwars-visualguide.com/assets/img/characters/${id}.jpg`
    return (
      <div className="person-details card">
        <img className="person-image"
          src={url} />

        <div className="card-body">
          <h4>{name}</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <span className="term">Gender</span>
              <span>{gender}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Birth Year</span>
              <span>43</span>
            </li>
            <li className="list-group-item">
              <span className="term">Eye Color</span>
              <span>{eyeColor}</span>
            </li>
          </ul>
        </div>
      </div>
    )
  }
}
